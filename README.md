iBashlib
========
iBashLib est une tentative de normalisation et de mise en place de processus de développement pour des fonctions utilitaires au format Bash.
Le principe est de toujours charger la 1re librairie « main.bashlib », puis de charger les librairies supplémentaires nécessaires au développement d’un script, d’un outil.

En résumé, iBashLib est un cadre de développement d’utilitaires Bash. Vous pouvez soit simplement l’utiliser soit ajouter de nouvelles librairies s’appuyant sur ses principes.


Procédure d'installation
------------------------
```
$ git clone http://gitlab.com/blacksmurf/ibashlib.git
```

Dans un environnement Bash pour démarrer iBashlib, exécutez :

```
$ source ibashlib/lib/main.bashlib
```

puis pour afficher l'aide, tapez :

```
$ iBashHelp
```


Pour afficher la liste des librairies disponibles, tapez :

```
$ getLibraries
```


Pour afficher les fonctions d'une librairie, tapez :

```
$ getFunctions <library_name.bashlib>
```

Pour charger une nouvelle librairie, tapez simplement :

```
$ isource <library_name.bashlib>
```


Pour charger la coloration syntaxique dans VIM, il faut éditer le fichier ~/.vimrc :

```
syntax on
filetype on
au BufNewFile,BufRead *.bashlib set filetype=sh
```


Prérequis
---------

Bash >= 3.x


Compatibilité
-------------

- GNU/Linux
- OSX & macOS
- Windows 10 (avec le bash Ubuntu)


Licence
-------

La licence est une licence Creative Commons : Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
Elle prévoit la copie et la distribution sur n'importe quel support dans des conditions de gratuité,
et l'interdiction de la revente, de l'appropriation ou de la modification des sources de ce projet.
