# Liste des changements
# Projet:       ibashlib
# Description:  Bibliothèque de fonctions Bash
# Société:      Ministère de l'Education nationale
# Auteurs:
- Julien Buonocore (julien.buonocore@ac-aix-marseille.fr)
- Isabelle Mahnich (isabelle.mahnich@ac-aix-marseille.fr)
- Benjamin Radici (benjamin.radici@ac-aix-marseille.fr)

## [0.13] - 2018-04-06
### Corrections
- nouveau système de versions
- divers correctifs

## [0.12] - 2018-03-28
### Corrections
- modification du nom des fonctions ODI
- suppression de la contrainte d'être ROOT pour démarrer ou arrêter une instance
- correctif dans la recherche de la home directory du binaire DB2

### Ajouts
- nouvelles fonctions ODI : import de scénarios, export des logs et suppression de scénarios
- nouvelle fonction "incrementVersion" dans la bibliothèque "utils"
- nouvelle fonction de "cataloguage" de noeud et de base de données pour DB2 en mode TCP
- création d'une instance DB2 en mode client

## [0.11] - 2018-03-22
### Corrections
- alignements avec dbtools
- correctifs divers sur la norme

### Ajouts
- nouvelle variable IBASHLIB_LANG
